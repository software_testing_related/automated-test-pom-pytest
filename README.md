# 自动化测试POM+pytest

#### 介绍
基于python+selenium+pytest+Allure+数据驱动实现的pom模式的UI自动化测试案例


#### 安装教程

1.  需要在运行的机器上安装python、webdriver、allure
2.  剩下的运行什么，编辑器会提示显示波浪线，直接安装即可

#### 使用说明

1.  运行run.py即可，会在对应的目录里生成测试报告，测试数据不可删