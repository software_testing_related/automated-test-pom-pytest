import os
import pytest

if __name__ == '__main__':
    pytest.main(['-s', './pytestdemo/testPage/testCase.py', '--alluredir', './pytestdemo/allureResults'])

    os.system('allure generate ./pytestdemo/allureResults -o ./pytestdemo/reports --clean')