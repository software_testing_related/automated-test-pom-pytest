# 人生苦短，我用python
# @Time : 2021/9/17 8:47
# @Author : shiChao
# @File : loadIni.py
# @Desc : 读取ini配置文件

import os
import sys
import configparser
import threading


class LoadIni:
    _instance_lock = threading.Lock()

    # 初始化
    def __new__(cls, *args, **kwargs):
        if not hasattr(LoadIni, "_instance"):
            with LoadIni._instance_lock:
                if not hasattr(LoadIni, "_instance"):
                    LoadIni._instance = object.__new__(cls)
        return LoadIni._instance

    def __init__(self, conf_name):
        basedir = os.path.dirname(os.path.dirname(__file__))
        sys.path.append(basedir)
        test_config = os.path.join(basedir, "config", conf_name)
        self.conf = configparser.RawConfigParser()
        self.conf.read(test_config, encoding='utf-8')

    # 获取配置
    def getConfig(self, section, option):
        return self.conf.get(section, option)
