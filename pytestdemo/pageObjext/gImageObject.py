"""
    这个对象实现从首页选择挂图，搜索 病毒 关键字
"""

import time
from pytestdemo.base.basePage import BasePage
from selenium.webdriver.common.by import By
from selenium import webdriver


class GImageObject(BasePage):
    # 首页
    url = 'https://cloud.kepuchina.cn/'

    # 点击下拉框选择挂图
    span = (By.CLASS_NAME, 'type-content')
    # guaTu = (By.XPATH, '/html/body/div[1]/div/section[1]/div/div/div[1]/ul/li[4]/a')
    guaTu = (By.XPATH, '/html/body/div[1]/div/section[1]/div/div/div[1]/ul/li[4]')

    # 搜索框 搜索按钮
    s = (By.CLASS_NAME, '_sKey')
    clickButton = (By.CLASS_NAME, 'search_icon')

    def operation(self, s):
        self.visit(self.url)
        self.click(self.span)
        self.click(self.guaTu)
        self.input(self.s, s)
        self.click(self.clickButton)


# if __name__ == '__main__':
#     driver = webdriver.Chrome("C:\Program Files\chromedriver.exe")
#     search = '病毒'
#     index = GImageObject(driver)
#     index.operation()
#     time.sleep(2)

# # 根据页面规则，page属性里倒数第三个 a 标签为总页数 @TODO 应该封装
# totalPage = driver.find_element_by_class_name("page").find_elements_by_tag_name("a")[-3].text
# total = int(totalPage)
# index = 1
#
# # 点击下一页
# while index <= total:
#     # 跳转到第二页
#     if index == total:
#         break
#
#     index += 1
#     current = str(index)
#     next_page = driver.find_element_by_class_name("page").find_element_by_link_text(current)
#
#     next_page.click()
#     time.sleep(1)
