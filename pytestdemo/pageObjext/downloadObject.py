import time

from selenium.webdriver.common.by import By
from pytestdemo.base.basePage import BasePage
from selenium import webdriver


class DownloadObject(BasePage):
    # 网址
    url = 'https://cloud.kepuchina.cn/newSearch/wallList?sort_rule=0'

    #
    downloadIcon = (By.XPATH, '/html/body/div[1]/section[2]/div/div[2]/div/div[1]/div[2]/p[1]/a/i')

    checkIcon = (By.XPATH, '//*[@id="layui-layer2"]/div[3]/a')

    def download(self):
        self.visit(self.url)
        self.click(self.downloadIcon)