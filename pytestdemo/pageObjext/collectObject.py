"""
    挂图收藏
"""

import time
from pytestdemo.base.basePage import BasePage
from selenium.webdriver.common.by import By


class CollectObject(BasePage):
    # 首页
    # url = 'https://cloud.kepuchina.cn/'

    # 点击下拉框选择挂图
    span = (By.XPATH, '/html/body/div[1]/section[2]/div/div[2]/div/div[1]/div[1]/p[1]')

    def collect(self):
        # self.visit(self.url)
        self.click(self.span)
