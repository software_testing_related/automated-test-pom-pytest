import time

from pytestdemo.base.basePage import BasePage
from selenium.webdriver.common.by import By
from selenium import webdriver

class LoginObject(BasePage):

    # 登录网址
    url = 'https://cloud.kepuchina.cn/account/login'

    # 手机号和密码
    mobile = (By.ID, 'userName')
    password = (By.ID, 'password')
    button = (By.CLASS_NAME, '_loginBtn')

    def login(self, mobile, password):
        self.visit(self.url)
        self.input(self.mobile, mobile)
        self.input(self.password, password)
        self.click(self.button)
