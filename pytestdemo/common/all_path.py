# _*_coding:utf-8 _*_
# @Time　　:2022/2/23 14:50
# @Author　 : shichao
# @File　　  :all_path.py
# @Software  :PyCharm
import os

base_path = os.path.dirname(os.path.dirname(__file__))

dataPath = os.path.join(base_path, 'data')
configPath = os.path.join(base_path, 'config')
logPath = os.path.join(base_path, 'outputs', 'logs')
picturePath = os.path.join(base_path, 'outputs', 'picture')
reportsPath = os.path.join(base_path, 'outputs', 'reports')
