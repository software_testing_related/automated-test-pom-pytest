import logging
import time
import datetime
from telnetlib import EC
from lib.log import logger
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait
from common.all_path import picturePath


class BasePage:

    # 构造函数
    def __init__(self, driver):
        self.driver = driver

    # 访问
    def visit(self, url):
        self.driver.get(url)

    # 查找元素（单）
    def find_element(self, *loc, doc="") -> WebElement:
        """
        查找单个元素方法
        :type doc: 定位元素界面位置：例如 首页
        :type loc: 元素定位
        """
        try:
            ele = self.driver.find_element(*loc)

            logger.info("{} 页面查找元素 {}成功！".format(doc, loc))
            return ele
        except Exception as e:
            logger.error('{} 定位元素 {}出现未知错误！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))

    # 查找属性（多）
    def find_elements(self, *loc, doc=''):
        """
        查找多个元素方法
        """
        try:
            logger.info("{} 页面开始查找一组元素{}".format(doc, loc))
            WebDriverWait(self.driver, 20, 0.5).until(EC.visibility_of_element_located(loc))
            ele = self.driver.find_elements(*loc)
            logger.info("{} 页面查找元素{}成功！".format(doc, loc))
            return ele
        except Exception as e:
            logger.error('{} 定位元素 {}出现未知错误！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))

    # 点击属性
    def click_element(self, *loc, doc=''):
        ele = self.find_element(*loc, doc=doc)
        try:
            logger.info("{} 页面点击元素{}".format(doc, loc))
            ele.click()
            logger.info("{} 页面点击元素{}成功！".format(doc, loc))
        except Exception as e:
            logger.error('{} 点击元素 {}失败！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))

    # 发送数据
    def send_keys(self, *loc, value, doc=''):
        """
        send_keys方法
        """
        ele = self.find_element(*loc, doc=doc)

        try:
            logger.info("{} 页面输入框输入 {}".format(doc, value))
            ele.clear()
            ele.send_keys(value)
            logger.info("{} 页面输入框输入 {} 成功！".format(doc, value))
        except Exception as e:
            logger.error('{} 定位元素 {}出现未知错误！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))

    def is_toast_exist(self, text, doc=''):
        """is toast exist, return True or False
        :Agrs:
            - text   - 页面上看到的文本内容
        :Usage:
            is_toast_exist("看到的内容")
        """
        try:
            toast_loc = (By.XPATH, ".//*[contains(@text,'%s')]" % text)
            logger.info("{} 页面开始查找 toast {}".format(doc, toast_loc))
            WebDriverWait(self.driver, 20, 0.5).until(EC.presence_of_element_located(toast_loc))
            logger.info("{} 页面开始查找 toast {} 成功！".format(doc, toast_loc))
            return True
        except Exception as e:
            logger.error('{} 页面toast {} 未出现！ 错误为：{}'.format(doc, text, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))
            return False

    def is_element_exist(self, *loc, doc=''):
        try:
            logger.info("{} 页面开始查找元素 {}".format(doc, loc))
            WebDriverWait(self.driver, 10, 0.5).until(EC.visibility_of_element_located(loc))
            logger.info("{} 页面查找元素 {}成功！".format(doc, loc))
            return True
        except Exception as e:
            logger.exception('{} 定位元素 {}出现未知错误！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))
            return False

    def get_text(self, *loc, doc=''):
        ele = self.find_element(*loc, doc=doc)
        try:
            logger.info("{} 页面开始获取 {} 文本".format(doc, loc))
            text = ele.text
            logger.info("{} 页面获取文本 {}成功！".format(doc, loc))
            return text
        except Exception as e:
            logger.exception('{} 定位元素 {}出现未知错误！ 错误为：{}'.format(doc, loc, e))
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))
            return None

    def get_screenshot(self, doc):
        """
        截图
        """
        logger.info("开始进行截图..")
        now = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
        pic_name = picturePath + r"\{}".format(doc) + now + '.png'
        self.driver.get_screenshot_as_file(pic_name)
        logger.info("截图成功！图片名称为:{}".format(pic_name))
        # with open(pic_name, mode='rb') as f:
        #     file = f.read()
        # allure.attach(file, doc, allure.attachment_type.PNG)
        return pic_name

    def result_assert(self, res, expected, doc=''):
        try:
            assert res == expected
        except AssertionError:
            screen_name = self.get_screenshot(doc)
            logger.info("截图成功，图片为：{}".format(screen_name))
            raise