import time
import pytest
from selenium import webdriver
from pytestdemo.pageObjext.collectObject import CollectObject
from pytestdemo.pageObjext.loginObject import LoginObject
from pytestdemo.pageObjext.gImageObject import GImageObject
from pytestdemo.config.loadYaml import loadYaml


class TestCase():

    def setup_class(cls) -> None:
        # 选择对应的包
        cls.driver = webdriver.Chrome("C:\Program Files\chromedriver.exe")

        cls.lg = LoginObject(cls.driver)
        cls.gImage = GImageObject(cls.driver)
        cls.collect = CollectObject(cls.driver)

    def teardown_class(cls) -> None:
        cls.driver.quit()

    @pytest.mark.parametrize('userData', loadYaml('./pytestdemo/data/user_test.yaml'))
    def test_1_login(self, userData):

        self.lg.login(userData['mobile'], userData['password'])

        time.sleep(2)

    # @pytest.mark.parametrize()
    def test_2_search(self):
        self.gImage.operation('病毒')
        time.sleep(2)


if __name__ == '__main__':
    pytest.main()
